# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2014, 2017, 2018, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2022-08-20 17:28+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/main.qml:23
#, kde-format
msgid ""
"The system must be restarted before changes to the middle-click paste "
"setting can take effect."
msgstr ""

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Restart"
msgstr ""

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Visual behavior:"
msgstr "Visuel opførsel:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:47 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "Vis informative værktøjstip når under musen"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:58 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "Vis visuel feedback for statusændringer"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Animation speed:"
msgstr "Animationshastighed:"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Langsom"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Øjeblikkelig"

#: package/contents/ui/main.qml:120
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "Klik på filer eller mapper:"

#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "Åbner dem"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Markér ved at klikke på elementets markeringsmarkør"

#: package/contents/ui/main.qml:143
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Markerer dem"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Åbn med dobbeltklik i stedet"

#: package/contents/ui/main.qml:171
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "Klik på rullebjælke-sporet:"

#: package/contents/ui/main.qml:172
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "Ruller en side op eller ned"

#: package/contents/ui/main.qml:184
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "Midterklik for at rulle til den placering der blev klikket på"

#: package/contents/ui/main.qml:194
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "Ruller til den placering der blev klikket på"

#: package/contents/ui/main.qml:213
#, kde-format
msgid "Middle Click:"
msgstr ""

#: package/contents/ui/main.qml:215
#, kde-format
msgid "Paste selected text"
msgstr ""

#: package/contents/ui/main.qml:232
#, kde-format
msgid "Touch Mode:"
msgstr "Touch-tilstand:"

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "Aktivér automatisk efter behov"

#: package/contents/ui/main.qml:234 package/contents/ui/main.qml:263
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "Aldrig aktiveret"

#: package/contents/ui/main.qml:246
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"Touch-tilstand aktiveres automatisk når systemet registrerer en touchskærm "
"men ingen mus eller touchpad. For eksempel: Når en transformerbar laptops "
"tastatur vendes rundt eller frakobles."

#: package/contents/ui/main.qml:251
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "Altid aktiveret"

#: package/contents/ui/main.qml:276
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"I touch-tilstand vil mange elementer i brugerfladen blive større, for bedre "
"at imødekomme interaktion med touch."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "Enkeltklik for at åbne filer"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "Animationshastighed"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "Venstreklik på rullebjælkesporet flytter rullebjælken med en side"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "Skift automatisk til touch-optimeret tilstand"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr ""

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"Vis et on-screen-display for at indikere statusændringer såsom lysstyrke "
"eller lydstyrke."

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "OSD ved layout-skift"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "Vis en pop-op ved layout-skift"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "General Behavior"
#~ msgstr "Generel opførsel"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr ""
#~ "Systemindstillinger-modul til at indstille arbejdsområdets generelle "
#~ "opførsel."

#~ msgid "Furkan Tokac"
#~ msgstr "Furkan Tokac"

#~ msgid "Click behavior:"
#~ msgstr "Klikopførsel:"

#~ msgid "Double-click to open files and folders"
#~ msgstr "Dobbeltklik for at åbne filer og mapper"

#~ msgid "Select by single-clicking"
#~ msgstr "Markér ved enkeltklik"

#~ msgid "Double-click to open files and folders (single click to select)"
#~ msgstr "Dobbeltklik for at åbne filer og mapper (enkeltklik for at markere)"

#~ msgid "Plasma Workspace global options"
#~ msgstr "Globale indstillinger for Plasma arbejdsområde"

#~ msgid "(c) 2009 Marco Martin"
#~ msgstr "(c) 2009 Marco Martin"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Maintainer"
#~ msgstr "Vedligeholder"

#, fuzzy
#~| msgid "Show Informational Tips:"
#~ msgid "Show Informational Tips"
#~ msgstr "Vis informative tips:"
